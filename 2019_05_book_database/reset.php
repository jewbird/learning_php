<?php

require_once 'inc/database.inc.php';

$db->query('DROP TABLE IF EXISTS books');

$db->query(
    'CREATE TABLE books (
        id INTEGER PRIMARY KEY AUTO_INCREMENT,
        title VARCHAR(255),
        author VARCHAR(255)
    ) DEFAULT CHARSET = utf8'
);

$db->query(
    'INSERT INTO books (title, author) VALUES
        ("The Origin Of Species", "Charles Darwin")'
);
<?php

$options = [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true
];

$db = new PDO(
    'mysql:host=localhost;dbname=books',
    'nina',
    'nina',
    $options
);

$db->query('SET NAMES utf8');
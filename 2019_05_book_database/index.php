<?php

require_once 'inc/functions.inc.php';

spl_autoload_register('autoloadControllers');
spl_autoload_register('autoloadEntities');

require_once 'inc/database.inc.php';

Book::dbConnect($db);

$controller = $_GET['controller'] ?? 'index';
$action = $_GET['action'] ?? 'index';

$controllerName = ucfirst($controller) . 'Controller';
$controllerFile = 'src/Controllers/' . $controllerName . '.php';

if (file_exists($controllerFile)) {
    require_once $controllerFile;
    
    $requestController = new $controllerName();
    $requestController->run($action);
} else {
    echo "404 Not Found";
    exit;
}
<?php

class Book
{
    protected $id     = 0;
    protected $title  = '';
    protected $author = 0;
    
    protected static $db;
    
    /***************CONSTRUCTOR*******************/
    
    public function __construct(array $data = [])
    {
        $this->setData($data);
    }
    
    public function setData(array $data)
    {
        if ($data) {
            foreach ($data as $k => $v) {
                $setterName = 'set' . ucfirst($k);
                if (method_exists($this, $setterName)) {
                    $this->$setterName($v);
                }
            }
        }
    }
    
    /*****************GET&SET*********************/
    
    public function getId()
    {
        return $this->id;
    }
    
    public function setId($id)
    {
        $this->id = $id;
    }
    
    public function getTitle()
    {
        return $this->title;
    }
    
    public function setTitle($title)
    {
        $this->title = $title;
    }
    
    public function getAuthor()
    {
        return $this->author;
    }
    
    public function setAuthor($author)
    {
        $this->author = $author;
    }
    
    /**************ACTIVE RECORD******************/
    
    public static function findAll()
    {
        $sql = 'SELECT * FROM books';
        $query = self::$db->query($sql);
        $query->setFetchMode(PDO::FETCH_CLASS, 'Book');
        
        return $query->fetchAll();
    }
    
    public static function find($id)
    {
        $sql = 'SELECT * FROM books WHERE id = ?';
        $query = self::$db->prepare($sql);
        $query->execute([$id]);
        $query->setFetchMode(PDO::FETCH_CLASS, 'Book');
        
        return $query->fetch();
    }
    
    public function save(Book $book)
    {
        if ($this->getId() > 0) {
            $this->update();
        } else {
            $this->insert();
        }
    }
    
    protected function insert()
    {
        $sql = 'INSERT INTO books (title, author)'
             . ' VALUES (:title, :author)';
        $query = self::$db->prepare($sql);
        
        $data = get_object_vars($this);
        unset($data['id']);
        $query->execute($data);
        
        $this->id = self::$db->lastInsertId();
    }
    
    protected function update()
    {
        $sql = 'UPDATE books SET title = :title, author = :author'
             . ' WHERE id = :id';
        $query = self::$db->prepare($sql);
        $query->execute(get_object_vars($this));
    }
    
    public function delete()
    {
        $sql = 'DELETE FROM books WHERE id = ?';
        $query = self::$db->prepare($sql);
        $query->execute([$this->getId()]);
        
        $this->id = 0;
    }
    
    /**************DB CONNECTION******************/
    
    public static function dbConnect(PDO $db)
    {
        self::$db = $db;
    }
    
}
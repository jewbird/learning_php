<?php

class IndexController extends AbstractBase
{
    /************indexAction.tpl.php*****************/

    protected function indexAction()
    {
        $this->addContext('books', Book::findAll());
    }
    
    /*************viewAction.tpl.php*****************/
    
    protected function viewAction()
    {
        $this->addContext('book', Book::find($_GET['id']));
    }
    
    /*************newAction.tpl.php******************/
    
    protected function newAction()
    {
        if ($_POST) {
            $book = new Book($_POST);
            $book->save($book);
            redirect('index.php');
        }
    }
    
    /****************deleteAction********************/
    
    protected function deleteAction()
    {
        $book = Book::find($_GET['id']);
        $book->delete();
        redirect('index.php');
    }
    
    /************updateAction.tpl.php****************/
    
    protected function updateAction()
    {
        if (isset($_GET['id'])) {
            $book = Book::find($_GET['id']);
        }
        
        if ($_POST) {
            $book = Book::find($_POST['id']);
            $book->setTitle($_POST['title']);
            $book->setAuthor($_POST['author']);
            $book->save($book);
            redirect('index.php');
        }
        
        $this->addContext('book', $book);
    }
}
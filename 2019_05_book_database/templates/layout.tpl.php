<!DOCTYPE html>
<html>
    
<head>
    <meta charset="utf-8" />
    <title>My Books</title>
</head>

<body>
    
    <nav>
        <ul id="nav">
            <li><a href="index.php">Home</a></li>
            <li><a href="index.php?action=new">New Book</a></li>
        </ul>
    </nav>
    
    <?php require $template; ?>
    
</body>

</html>
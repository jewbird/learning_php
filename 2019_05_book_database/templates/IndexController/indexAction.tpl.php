<ul>
    <?php foreach ($books as $id => $book): ?>
        <li>
            <h3><?= clean($book->getTitle()) ?></h3>
            <a href="index.php?action=delete&amp;id=<?= (int)$book->getId() ?>">
                <button>Delete</button>
            </a>
            <a href="index.php?action=update&amp;id=<?= (int)$book->getId() ?>">
                <button>Update</button>
            </a>
            <a href="index.php?action=view&amp;id=<?= (int)$book->getId() ?>">
                <button>View</button>
            </a>
        </li>
    <?php endforeach; ?>
</ul>
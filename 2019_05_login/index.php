<?php

session_start();
$pdo = new PDO('mysql:host=localhost;dbname=login', 'nina', 'nina');
$_SESSION['logged_in'] = false;

// login action

if(isset($_GET['login'])) {
	$email = $_POST['email'];
	$password = $_POST['password'];
	
	$statement = $pdo->prepare("SELECT * FROM users WHERE email = :email");
	$result = $statement->execute(array('email' => $email));
	$user = $statement->fetch();
	
	// check password
	if ($user !== false && password_verify($password, $user['password'])) {
		$_SESSION['userid'] = $user['id'];
		$pos = strpos($user['email'], '@');
		$_SESSION['username'] = ucfirst(substr($user['email'], 0, $pos));
	}
}

if(isset($_SESSION['userid'])) {
	$_SESSION['logged_in'] = true;
}

?>

<!DOCTYPE=html>
<html>
<head>
<title>Login</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body style="background-color:black; color:white;">

<?php require "nav.php";

if ($_SESSION['logged_in'] === false) { ?>

<h1>My login page :-)</h1>

<?php } else { ?>

<h1><?php echo $_SESSION['username']; ?> is logged in!</h1>

<?php } ?>

</body>
</html>


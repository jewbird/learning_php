<?php

session_start();
$pdo = new PDO('mysql:host=localhost;dbname=login', 'nina', 'nina');

?>

<!DOCTYPE html>
<html>
<head>
	<title>Register</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body style="background-color:black; color:white;">

<?php

require "nav.php";

$showForm = true; // variable if the form will be displayed or not

if (isset($_GET['register'])) {
	$error = false;
	$email = $_POST['email'];
	$password = $_POST['password'];
	$password2 = $_POST['password2'];
	
	if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		echo 'Please enter a valid e-mail address<br>';
		$error = true;
	}
	if(strlen($password) == 0) {
		echo 'Please enter a password<br>';
		$error = true;
	}
	if ($password != $password2) {
		echo 'Passwords do not match<br>';
		$error = true;
	}
	
	// check if email address doesn't exist already
	if(!$error) {
		$statement = $pdo->prepare("SELECT * FROM users WHERE email = :email");
		$result = $statement->execute(array('email' => $email));
		$user = $statement->fetch();
		
		if ($user !== false) {
			echo 'This e-mail address is already in use<br>';
			$error = true;
		}
	}
	
	// no errors, user will be registered
	if (!$error) {
		$password_hash = password_hash($password, PASSWORD_DEFAULT);
		
		$statement = $pdo->prepare("INSERT INTO users (email, password) VALUES (:email, :password)");
		$result = $statement->execute(array('email' => $email, 'password' => $password_hash));
		
		if ($result) {
			echo 'Registration was successful!';
			$showForm = false;
		} else {
			echo 'Something went wrong :( <br>';
		}
	}
}

if ($showForm) { ?>

<form class="form-group" action="?register=1" method="post">
	<label for="email" class="mt-2 mb-0">Your E-Mail Address</email>
	<input type="email" class="form-control" name="email" id="email" placeholder="E-Mail" required>
	<label for="password" class="mt-2 mb-0">Choose Password</label>
	<input type="password" class="form-control" name="password" id="password" placeholder="Password" required>
	<label for="password2" class="mt-2 mb-0">Repeat Password</label>
	<input type="password" class="form-control" name="password2" id="password2" placeholder="Repeat Password" required>
	<input type="submit" class="btn btn-primary mt-2" value="Submit">
</form>

<?php } ?>

</body>
</html>






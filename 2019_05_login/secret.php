<?php

session_start();
if(!isset($_SESSION['userid'])) {
    exit('Please <a href="login.php">login</a>');
}

// request user id from login
$userid = $_SESSION['userid'];
$username = $_SESSION['username'];

echo 'Hello ' . $username . ' :-)';
?>

<button><a href="logout.php">Logout</a></button>

<?php

session_start();

?>

<!DOCTYPE html>
<html>
<head>
	<title>Edit</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body style="background-color:black; color:white;">

<?php require "nav.php";

if ($_SESSION['logged_in'] === true) { ?>

<h1>Hello <?php echo $_SESSION['username']; ?>, you can edit your page here...</h1>

<?php

} else { ?>

	<h1>Please log in to continue</h1>
	
<?php } ?>
<nav class="navbar navbar-expand-lg navbar-light bg-info">
<a class="navbar-brand" href="index.php"><img src="img/brand.png" width=45 height=30></a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
	<span class="navbar-toggler-icon"></span>
</button>

<div class="collapse navbar-collapse" id="navbarTogglerDemo03">
	<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
		<?php
			if ($_SESSION['logged_in'] === true) { ?>
				<li class="nav-item"><a class="nav-link" href="edit.php">Edit</a></li>
				<li class="nav-item"><a class="nav-link" href="logout.php">Logout</a></li>
		<?php } else { ?>
				<li class="nav-item"><a class="nav-link" href="register.php">Register</a></li>
	</ul>
				<form class="form-inline my-2 my-lg-0" action="index.php?login=1" method="post">
					<input class="form=control mr-sm-2" type="email" name="email" placeholder="E-Mail" required>
					<input class="form=control mr-sm-2" type="password" name="password" placeholder="Password" required>
					<input type="submit" value="login" class="btn btn-light my-2 my-sm-0">
				</form>
		<?php } ?>
</div>
</nav>
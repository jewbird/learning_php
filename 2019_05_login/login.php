<?php

session_start();
$pdo = new PDO('mysql:host=localhost;dbname=login', 'nina', 'nina');

if(isset($_GET['login'])) {
    $email = $_POST['email'];
    $password = $_POST['password'];
    
    $statement = $pdo->prepare("SELECT * FROM users WHERE email = :email");
    $result = $statement->execute(array('email' => $email));
    $user = $statement->fetch();
    
    // check password
    if ($user !== false && password_verify($password, $user['password'])) {
        $_SESSION['userid'] = $user['id'];
        $pos = strpos($user['email'], '@');
		$_SESSION['username'] = ucfirst(substr($user['email'], 0, $pos));
        exit('Login successful. Continue to <a href="secret.php">personal page</a>');
    } else {
        $errorMessage = "Invalid e-mail or password<br>";
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Login</title>
</head>
<body>

<?php
if(isset($errorMessage)) {
    echo $errorMessage;
}
?>

<form action="?login=1" method="post">
    <input type="email" name="email" placeholder="E-Mail">
    <input type="password" name="password" placeholder="Password">
    <input type="submit" value="Submit">
</form>
</body>
</html>

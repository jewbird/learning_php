<style>body {font-family: "monospace"}</style>
<?php

echo '<hr>';

/***********************************************************************/
/******************Output random word from list*************************/

echo '*****************Output random word from list********************';
echo '<br>';

$distro = ["Ubuntu", "Mint", "Debian", "Fedora", "SUSE",
          "elementaryOS", "Zorin", "CentOS", "Arch",
          "Manjaro", "Solus"];

$x = rand(0, count($distro) - 1);

echo $distro[$x];
echo '<hr>';

/***********************************************************************/
/***************************How many days*******************************/

echo '*********How many days have passed between 1961 and 1989?********';
echo '<br>';

$sec = ( mktime(23,59,59,11,9,1989) - mktime(0,0,0,8,13,1961) );
$day = 60 * 60 * 24;

$time = $sec / $day;
$time = round($time);
echo $time . ' Days';

echo '<hr>';

/***********************************************************************/
/******************************Date validation**************************/

echo '********Check if random date between 1900 and 2100 exsists*******';
echo '<br>';

$day = 29;
$month = 2;
$year = 2020;

$okday = 0;
$okmonth = 0;
$okyear = 0;

$datok = 0;

if ($year > 1900 && $year < 2100) {
    $okyear = 1;
}

if ($month >=1 && $month <= 12) {
    $okmonth = 1;
}

switch ($month) {
    case 4:
    case 6:
    case 9:
    case 11:
         if ($day >= 1 && $day <= 30) {
             $okday = 1;
         }
         break;
    case 2:
        if ($year % 4 != 0) {
            if ($day >= 1 && $day <= 28) {
                $okday = 1;
            }
        } else {
            if ($day >= 1 && $day <= 29) {
                $okday = 1;
            }
        }
        break;
    default:
        if ($day >= 1 && $day <= 31) {
            $okday = 1;
        }
}

if ($okday == 1 && $okmonth == 1 && $okyear == 1) {
    $datok = 1;
}

if ($okday == 1 && $okmonth == 1 && $okyear == 1) {
	echo $day . '.' . $month . '.' . $year . ' is an existing date!';
} else {
	echo $day . '.' . $month . '.' . $year . ' is not an existing date!';
}

echo '<hr>';
/***********************************************************************/
/********************Output three unique random numbers*****************/
echo '***************Output three unique random numbers****************';
echo '<br>';

$x = rand(1, 3);
do {
    $y = rand(1, 3);
    $z = rand(1, 3);
} while ($x == $y || $x == $z || $y == $z);

echo $x . 'x ' . $y . 'y ' . $z . 'z';

echo '<hr>';

/***********************************************************************/
/*********************Output 1 2 3 4 5 4 3 2 1 *************************/

echo '********************Output 1 2 3 4 5 4 3 2 1*********************';
echo '<br>';

$j = 1;
for ($i = 1; $i > 0; $i += $j) {
    echo $i . ' ';
    if ($i == 5) {
        $j = -1;
    }
}

echo '<hr>';

/***********************************************************************/
/***********************Output Fibonacci < 500**************************/

echo '****************************Fibonacci****************************';
echo '<br>';

echo 0 . ' ';
for ($a = 1, $b = 1; $a < 500; $a += $b) {
    echo $a . ' ';
    $b = $a - $b;
}

echo '<hr>';
